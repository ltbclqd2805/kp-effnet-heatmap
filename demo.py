import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np 
import cv2
import os
import requests
import json
import logzero
import time
import base64
import shutil
import math
import threading

from datetime import datetime
from argparse import ArgumentParser
from logzero import logger
from const import *

sess = tf.Session()
graph = tf.get_default_graph()
tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], 'saved_model/mbnv3s_realsyntheticdata')

images = graph.get_tensor_by_name("Placeholder:0")
is_training = graph.get_tensor_by_name("Placeholder_2:0")
_heatmap = graph.get_tensor_by_name("Sigmoid:0")
_scores = graph.get_tensor_by_name("Max:0")
_classes = graph.get_tensor_by_name("ArgMax:0")


def check_corner(img, threshold=0.8):
    height, width = img.shape[:2]
    r_h = height * 1.0 / IMAGE_SIZE
    r_w = width * 1.0 / IMAGE_SIZE

    img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))

    img = np.expand_dims(img, axis=0)
    
    heatmap, scores, classes = sess.run([_heatmap, _scores, _classes], feed_dict={images: img/255, is_training: False})

    mask_scores = (scores >= threshold).astype(int)
    scores = scores * mask_scores
    min_score = np.min(scores)
    
    classes += 1
    classes *= mask_scores
    hm = np.squeeze(heatmap)

    for i in range(NUM_CLASSES):
        idx = np.array(np.where(classes == i + 1))
        if idx.shape[1] == 0:
            return False

    return True

def detect_corner(img, threshold=0.8):
    old_img = img.copy()
    height, width = img.shape[:2]
    r_h = height * 1.0 / IMAGE_SIZE
    r_w = width * 1.0 / IMAGE_SIZE

    img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))

    img = np.expand_dims(img, axis=0)
    
    heatmap, scores, classes = sess.run([_heatmap, _scores, _classes], feed_dict={images: img/255, is_training: False})

    mask_scores = (scores >= threshold).astype(int)
    scores = scores * mask_scores

    classes += 1
    classes *= mask_scores

    img = old_img
    hm = np.squeeze(heatmap)

    for i in range(NUM_CLASSES):
        idx = np.array(np.where(classes == i+1))
        max_score = 0
        xs = -1
        ys = -1
        for j in range(idx.shape[1]):
            if scores[idx[0,j], idx[1,j], idx[2,j]] > max_score:
                ys = idx[1, j]
                xs = idx[2, j]
                max_score = scores[idx[0][j], idx[1][j], idx[2][j]]
        if xs != -1:
            print('Class %d, max_score %.2f, (%d, %d)'%(i, max_score, xs, ys))
            xs = (xs * DOWN_RATIO + DOWN_RATIO / 2) * r_w
            ys = (ys * DOWN_RATIO + DOWN_RATIO / 2) * r_h

            cv2.circle(img, (int(xs), int(ys)), 10, (255, 0, 0), 10)
    return img

def get_image_id(path):
    return os.path.split(path)[-1].split('.')[0]

def parse_arguments():
    parser = ArgumentParser()
    
    parser.add_argument('--url', default='https://api.vdsense.com')
    parser.add_argument('--retries', type=int, default=3)
    parser.add_argument('--no-errors', dest='skip_errors', action='store_false')
    parser.add_argument('--no-debug', action='store_true')
    parser.add_argument('--timeout', type=int, default=120)

    return parser.parse_args()


def create_request(path):
    with open(path, 'rb') as f:
        sb64 = base64.b64encode(f.read()).decode()

    return {
        'image': {
            'content': sb64
        },
        'features': {
            'type': 'IDENTITY_CARD_DETECTION'
        }
    }

def convert_preds(preds):
    resp = preds[0]['identityCardAnnotations']
    out = {}

    for item in resp:
        t = item['field']
        if t == 'identityCard':
            continue
        out[t] = item['description']
    return out

def send_request(detected_frame):
    cv2.imwrite('temp.jpg', detected_frame)

    args = parse_arguments()
    job_id = datetime.now().strftime('%Y-%m-%d_%Hh%Mm%Ss')

    url = args.url + '/ekyc/v1/images:annotate'

    try:
        batch = ['temp.jpg']
        reqs = list(map(create_request, batch))
        ids = list(map(get_image_id, batch))

        logger.info('Making request to %s' % url)

        reqf = {
            'requests': reqs
        }

        r = None
        for i in range(args.retries + 1):
            if i > 0:
                logger.warning('Retrying (%d/%d)' % (i, args.retries))
            
            req_start = datetime.now()
            try:
                r = requests.post(url, json=reqf, timeout=args.timeout)
            except requests.exceptions.Timeout:
                logger.error('Request timed out')
                break
            req_time = datetime.now() - req_start

            if r.status_code != 200:
                logger.error('Request failed with status code %d' % r.status_code)
            else:
                break
        
        if r is None or r.status_code != 200:
            logger.error('Exiting due to errors')
            exit_code = 11 if r is None else r.status_code
            exit(exit_code)

        responses = r.json()['responses']
        with open('result.json', 'w', encoding="utf-8") as f:
            f.write(json.dumps(responses[0], indent=2, ensure_ascii=False))
        logger.info('Request completed in %ds' % req_time.total_seconds())
        logger.info('Finished')
    except KeyboardInterrupt:
        logger.warning('Interrupted.')
        exit(-1)

    os.remove('temp.jpg')
    print(convert_preds(responses))


if __name__ == "__main__":

    cap = cv2.VideoCapture(1)
    cap.set(cv2.CAP_PROP_AUTOFOCUS, 0)
    cap.set(28, 15)

    cnt = 0

    appearance = 0
    detected_frame = None
    max_min_score = 0.
    list_frame = []

    break_frame = 0


    while(True):
        ret, frame = cap.read()
        cv2.imshow('frame', frame)
        # if appearance == 1:
        #     break_frame += 1
        #     if break_frame < 30:
        #         continue
        cnt += 1
        if cnt % 5 == 0:
            t = check_corner(frame)
            if t:
                appearance += 1
                if appearance == 4:
                    detected_frame = frame
                    # print(send_request(detected_frame))
                    req_thread = threading.Thread(send_request(detected_frame))
                    req_thread.start()
                    appearance = 0
                    break_frame = 0
            else:
                appearance = 0
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
