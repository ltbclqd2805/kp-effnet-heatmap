import tensorflow as tf


def vgg_small(images, is_training):
    endpoints = {}

    x = tf.nn.swish(tf.layers.conv2d(images, 8, 3, 2))
    endpoints['reduction_1'] = x

    x = tf.nn.swish(tf.layers.conv2d(x, 16, 3, 1, 'same'))
    x = tf.nn.swish(tf.layers.conv2d(x, 16, 3, 1, 'same'))
    x = tf.layers.max_pooling2d(x, 2, 2, 'same')
    endpoints['reduction_2'] = x

    x = tf.nn.swish(tf.layers.conv2d(x, 32, 3, 1, 'same'))
    x = tf.nn.swish(tf.layers.conv2d(x, 32, 3, 1, 'same'))
    x = tf.layers.max_pooling2d(x, 2, 2, 'same')
    endpoints['reduction_3'] = x

    x = tf.nn.swish(tf.layers.conv2d(x, 64, 3, 1, 'same'))
    x = tf.nn.swish(tf.layers.conv2d(x, 64, 3, 1, 'same'))
    x = tf.layers.max_pooling2d(x, 2, 2, 'same')
    endpoints['reduction_4'] = x

    x = tf.nn.swish(tf.layers.conv2d(x, 128, 3, 1, 'same'))
    x = tf.nn.swish(tf.layers.conv2d(x, 128, 3, 1, 'same'))
    x = tf.nn.swish(tf.layers.conv2d(x, 128, 3, 1, 'same'))
    x = tf.layers.max_pooling2d(x, 2, 2, 'same')
    endpoints['reduction_5'] = x

    return x, endpoints


def build_model_base(images, model_name, training):
    if True:
        features, endpoints = vgg_small(images, training)
        return features, endpoints