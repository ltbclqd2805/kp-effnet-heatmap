import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np 
import cv2

from model import Model, Mobilenetv3Model
from const import *


def export_saved_model():
    with tf.Session(config=tf.ConfigProto(allow_soft_placement=False)) as sess:
        model = Mobilenetv3Model(ann_train=None, ann_valid=None, session=sess, phase='test')
        print(model.images)
        print(model.is_training)
        print(model.kp_prediction)
        print(model.scores)
        print(model.classes)

        signature = tf.saved_model.signature_def_utils.predict_signature_def(
                    inputs={'image': model.images, 'is_training': model.is_training},
                    outputs={'kp': model.kp_prediction, 'scores': model.scores, 'classes': model.classes})

        builder = tf.saved_model.builder.SavedModelBuilder('./saved_model/mbnv3s_realsyntheticdata')
        builder.add_meta_graph_and_variables(
            sess=sess,
            tags=[tf.saved_model.tag_constants.SERVING],
            signature_def_map={
                tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
                    signature
            })
        builder.save()

export_saved_model()


# sess = tf.Session()
# graph = tf.get_default_graph()
# tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], 'saved_model/eff-b0-fixed-BN')

# images = graph.get_tensor_by_name("Placeholder:0")
# is_training = graph.get_tensor_by_name("Placeholder_2:0")
# _heatmap = graph.get_tensor_by_name("Sigmoid:0")
# _scores = graph.get_tensor_by_name("Max:0")
# _classes = graph.get_tensor_by_name("ArgMax:0")

# test_folder = '/media/cuongbka2805/Study/Python/idcard/id_card_classification/result/cccd_front/'
# kp_res = '/media/cuongbka2805/Study/Python/corner_detection/result'
# for fn in os.listdir(test_folder):
#     if fn.find('.CMT') >= 0 or fn.find('.txt') >= 0:
#         continue
#     if not os.path.isfile(os.path.join(kp_res, fn)):
#         continue
#     image_path = os.path.join(test_folder, fn)

#     img = cv2.imread(image_path)
    
#     height, width = img.shape[:2]
#     r_h = height * 1.0 / IMAGE_SIZE
#     r_w = width * 1.0 / IMAGE_SIZE

#     img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))

#     img = np.expand_dims(img, axis=0)
    
#     heatmap, scores, classes = sess.run([_heatmap, _scores, _classes], feed_dict={images: img/255, is_training: False})

#     # img = np.squeeze(img)

#     img = cv2.imread(image_path)
#     hm = np.squeeze(heatmap)

#     for i in range(NUM_CLASSES):
#         idx = np.array(np.where(classes == i))
#         max_score = 0
#         xs = 0
#         ys = 0
#         for j in range(idx.shape[1]):
#             if scores[idx[0,j], idx[1,j], idx[2,j]] > max_score:
#                 ys = idx[1, j]
#                 xs = idx[2, j]
#                 max_score = scores[idx[0][j], idx[1][j], idx[2][j]]
#         print('Class %d, max_score %.2f, (%d, %d)'%(i, max_score, xs, ys))
#         xs = (xs * DOWN_RATIO + DOWN_RATIO / 2) * r_w
#         ys = (ys * DOWN_RATIO + DOWN_RATIO / 2) * r_h

#         cv2.circle(img, (int(xs), int(ys)), 10, (255, 0, 0), 10)

#     plt.subplot(1, 2, 1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
#     img = cv2.imread(os.path.join(kp_res, fn))
#     plt.subplot(1, 2, 2), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
#     plt.show()
