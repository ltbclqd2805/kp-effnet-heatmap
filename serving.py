import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np 
import cv2
import time

from const import *

sess = tf.Session()
graph = tf.get_default_graph()
tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], 'saved_model/eff-b4-realsyntheticdata')

images = graph.get_tensor_by_name("Placeholder:0")
is_training = graph.get_tensor_by_name("Placeholder_2:0")
_heatmap = graph.get_tensor_by_name("Sigmoid:0")
_scores = graph.get_tensor_by_name("Max:0")
_classes = graph.get_tensor_by_name("ArgMax:0")

test_folder = '/media/cuongbka2805/Study/Python/idcard/id_card_classification/result/cccd_front/'
kp_res = '/media/cuongbka2805/Study/Python/corner_detection/result'
for fn in os.listdir(test_folder):
    if fn.find('.CMT') >= 0 or fn.find('.txt') >= 0:
        continue
    if not os.path.isfile(os.path.join(kp_res, fn)):
        continue
    image_path = os.path.join(test_folder, fn)

    start = time.time()
    img = cv2.imread(image_path)
    
    height, width = img.shape[:2]
    r_h = height * 1.0 / IMAGE_SIZE
    r_w = width * 1.0 / IMAGE_SIZE

    img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))

    img = np.expand_dims(img, axis=0)
    
    heatmap, scores, classes = sess.run([_heatmap, _scores, _classes], feed_dict={images: img/255, is_training: False})
    print(heatmap.shape)
    print(scores.shape) # batchsize x 28 x 28
    print(classes.shape) # batchsize x 28 x 28

    _batch_size = scores.shape[0]
    _output_size = scores.shape[1]

    img = cv2.imread(image_path)
    hm = np.squeeze(heatmap)

    for i in range(NUM_CLASSES):
        # mask = (classes == i).astype(int) # batchsize x 28 x 28
        # scores_mask = mask * scores # batchsize x 28 x 28
        # scores_mask_rs = np.reshape(scores_mask, (_batch_size, -1)) # batchsize x 784
        # scores_max = np.max(scores_mask_rs, axis=-1) # batchsize, 
        # scores_max_rs = np.repeat(scores_max, _output_size ** 2, 0).reshape(_batch_size, -1) # batchsize x 784

        # idx = np.array(np.where(scores_mask_rs == scores_max_rs)) # 2 x m


        mask = (classes == i).astype(int)
        max_score = np.max(scores * mask)
        idx_max = np.array(np.where(scores == max_score))
        xs = idx_max[2, 0]
        ys = idx_max[1, 0]

        xs = (xs * DOWN_RATIO + DOWN_RATIO / 2) * r_w
        ys = (ys * DOWN_RATIO + DOWN_RATIO / 2) * r_h

        cv2.circle(img, (int(xs), int(ys)), 10, (255, 0, 0), 10)
    print(time.time() - start)

    plt.subplot(1, 2, 1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    img = cv2.imread(os.path.join(kp_res, fn))
    plt.subplot(1, 2, 2), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.show()
