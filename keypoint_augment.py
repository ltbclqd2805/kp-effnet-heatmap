from data_aug.data_aug import *
from data_aug.bbox_util import *
import matplotlib.pyplot as plt
from PIL import Image
import cv2
import numpy as np
import os
import math
import random
import time
import imgaug as ia
import imgaug.augmenters as iaa

def rand(a=0, b=1):
    return np.random.rand() * (b - a) + a

def euclide_distance(p1, p2):
    return np.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

def find_coeffs(source_coords, target_coords):
    matrix = []
    for s, t in zip(source_coords, target_coords):
        matrix.append([t[0], t[1], 1, 0, 0, 0, -s[0]*t[0], -s[0]*t[1]])
        matrix.append([0, 0, 0, t[0], t[1], 1, -s[1]*t[0], -s[1]*t[1]])
    A = np.matrix(matrix, dtype=np.float)
    B = np.array(source_coords).reshape(8)
    res = np.dot(np.linalg.inv(A.T * A) * A.T, B)
    return np.array(res).reshape(8)

def find_covered_rectangle2(box):
    m = np.min(box, axis=0)
    xmin = min(m[0], m[2])
    ymin = min(m[1], m[3])

    m = np.max(box, axis=0)
    xmax = max(m[0], m[2])
    ymax = max(m[1], m[3])

    return (int(xmin), int(ymin), int(xmax), int(ymax))

def find_covered_rectangle(box):
    xmin = min(box[0:8:2])
    ymin = min(box[1:8:2])

    xmax = max(box[0:8:2])
    ymax = max(box[1:8:2])

    return (int(xmin), int(ymin), int(xmax), int(ymax))

def find_card_coordinates(box):
    center = [(box[2*i], box[2*i+1]) for i in range(4)]
    center = np.array(center, dtype=np.int32)
    return center


def find_boxes_from_points(coors):
    box = []
    for c in coors:
        box.append([c[0], c[1], c[0] + 1, c[1] + 1])
    return np.array(box)

def simple_augment(image, point, data_type):
    if data_type == 'valid':
        return image, point

    _image = image.copy()
    _point = point.copy()

    sometimes = lambda aug: iaa.Sometimes(0.5, aug)
    rarely = lambda aug: iaa.Sometimes(0.2, aug)

    seq = iaa.Sequential(
    [
        iaa.SomeOf((0, 5),
            [
                iaa.OneOf([
                    iaa.GaussianBlur((0, 3.0)),
                    iaa.AverageBlur(k=(2, 7)),
                    iaa.MedianBlur(k=(3, 11)),
                ]),

                iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)),

                iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)),

                sometimes(iaa.OneOf([
                    iaa.EdgeDetect(alpha=(0, 0.7)),
                    iaa.DirectedEdgeDetect(
                        alpha=(0, 0.7), direction=(0.0, 1.0)
                    ),
                ])),

                iaa.AdditiveGaussianNoise(
                    loc=0, scale=(0.0, 0.05*255), per_channel=0.5
                ),

                iaa.OneOf([
                    iaa.Dropout((0.01, 0.1), per_channel=0.5),
                    iaa.CoarseDropout(
                        (0.03, 0.15), size_percent=(0.02, 0.05),
                        per_channel=0.2
                    ),
                ]),

                iaa.Invert(0.05, per_channel=True), # invert color channels

                # Add a value of -10 to 10 to each pixel.
                iaa.Add((-10, 10), per_channel=0.5),

                # Change brightness of images (50-150% of original value).
                iaa.Multiply((0.5, 1.5), per_channel=0.5),

                # Improve or worsen the contrast of images.
                iaa.LinearContrast((0.5, 2.0), per_channel=0.5),

                iaa.Grayscale(alpha=(0.0, 1.0)),

                # In some images distort local areas with varying strength.
                sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.05)))
            ],
            # do all of the above augmentations in random order
            random_order=True
        )
    ],
    # do all of the above augmentations in random order
    random_order=True
    )
    print('Augment')
    image = seq(images=[image])[0]
    return image, point


def augment(image, point, data_type):
    if data_type == 'valid':
        return image, point

    # start = time.time()
    _image = image.copy()
    _point = point.copy()
    try:
        box = list(np.array(point).reshape((-1)))
        covered_rectangle = find_covered_rectangle(box)
        card_coors = find_card_coordinates(box)
        cropped = Image.fromarray(image)
        coeffs = find_coeffs(card_coors, [(0, 0), (400, 0), (400, 250), (0, 250)])
        cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)

        # Trapezoid augment
        if rand() < .5:
            offset_x = int(rand(1, 100))
            offset_y = int(offset_x / 2.)
            dst_coors = [(offset_x, offset_y), (400 - offset_x, offset_y), (350, 250 - offset_y), (50, 250 - offset_y)]
            coeffs = find_coeffs([(0, 0), (400, 0), (400, 250), (0, 250)], dst_coors)
            cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)
        # Parallelogram augment
        else:
            offset_x = int(rand(10, 100))
            offset_y = int(offset_x / 2.)
            dst_coors = [(offset_x, offset_y), (350, offset_y), (400 - offset_x, 250 - offset_y), (50, 250 - offset_y)]
            coeffs = find_coeffs([(0, 0), (400, 0), (400, 250), (0, 250)], dst_coors)
            cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)


        image = np.array(image)
        image[covered_rectangle[1]:covered_rectangle[3], covered_rectangle[0]:covered_rectangle[2]] = 255

        new_box = find_boxes_from_points(dst_coors).astype(np.float32)
        cropped, new_box = RandomRotate(180)(np.array(cropped).copy(), new_box.copy())

        covered_rectangle = find_covered_rectangle2(new_box)
        cmt = np.array(cropped)[covered_rectangle[1]:covered_rectangle[3], covered_rectangle[0]:covered_rectangle[2]]

        new_box[:, 0] -= covered_rectangle[0]
        new_box[:, 1] -= covered_rectangle[1]
        new_box[:, 2] -= covered_rectangle[0]
        new_box[:, 3] -= covered_rectangle[1]

        size = int(rand(120, 220))
        cmt, new_box = Resize(size)(cmt.copy(), new_box.copy())

        x_r = int(rand(0, 224 - size - 1))
        y_r = int(rand(0, 224 - size - 1))

        image[y_r:y_r+size, x_r:x_r+size, :] = cmt
        new_box[:, 0] += x_r
        new_box[:, 1] += y_r
        new_box[:, 2] += x_r
        new_box[:, 3] += y_r

        if rand() < .5:
            image, new_box = RandomHSV(100, 100, 100)(image.copy(), new_box.copy())

        if len(new_box) != 4:
            return _image, _point

        new_label = []
        for j in range(4):
            new_label.append((new_box[j][0], new_box[j][1]))

        return image, new_label
    except:
        return _image, _point
