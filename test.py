import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
import keypoint_augment
import random
from const import *


class Dataset:
    def __init__(self, annotation, data_type, batch_size):
        self.annotation = annotation
        self.data_type = data_type
        self.batch_size = batch_size
        self.duplicate_cccd = True
        if self.data_type == 'valid':
            self.duplicate_cccd = False

    @staticmethod
    def euclide_distance(p1, p2):
        return np.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

    @staticmethod
    def gaussian_k(x0, y0, sigma, width, height):
        x = np.arange(0, width, 1, float) ## (width,)
        y = np.arange(0, height, 1, float)[:, np.newaxis] ## (height,1)
        return np.exp(-((x-x0)**2 + (y-y0)**2) / (2*sigma**2))

    @staticmethod
    def generate_hm(target_size, landmarks, image_size, s=3):
        Nlandmarks = len(landmarks)
        hm = np.zeros((target_size, target_size, Nlandmarks), dtype = np.float32)
        for i in range(Nlandmarks):
            if not np.array_equal(landmarks[i], [-1,-1]):
                x = int(landmarks[i][0] * target_size * 1.0 / image_size)
                y = int(landmarks[i][1] * target_size * 1.0 / image_size)
                hm[:,:,i] = Dataset.gaussian_k(x, y, s, target_size, target_size)
            else:
                hm[:,:,i] = np.zeros((target_size,target_size))
        return hm

    def gen(self):
        np.random.shuffle(self.annotation)
        batch_image = []
        batch_hm = []
        for idx in range(len(self.annotation)):
            if len(batch_image) == self.batch_size:
                batch_image = []
                batch_hm = []
            line = self.annotation[idx]
            line = line.replace('\n', '').split(' ')
            image_path = os.path.join(SRC_DATA_DIR, line[0] + '.jpg')

            if not os.path.isfile(image_path):
                continue

            img = cv2.imread(image_path)
            
            height, width = img.shape[:2]

            points = []
            for i in range(NUM_CLASSES):
                x, y = int(line[i * 2 + 1]), int(line[i * 2 + 2])
                x = x * IMAGE_SIZE * 1.0 / width
                y = y * IMAGE_SIZE * 1.0 / height
                points.append((x, y))

            img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))

            if image_path.find('cccd') >= 0 and self.duplicate_cccd:
                target_size = int(IMAGE_SIZE / DOWN_RATIO)
                heatmap = self.generate_hm(target_size, points, image_size=IMAGE_SIZE, s=np.sqrt(DOWN_RATIO/2))
                batch_image.append(img)
                batch_hm.append(heatmap)

                for i in range(3):
                    img, points = keypoint_augment.simple_augment(img, points, self.data_type)
                    heatmap = self.generate_hm(target_size, points, image_size=IMAGE_SIZE, s=np.sqrt(DOWN_RATIO/2))
                    batch_image.append(img)
                    batch_hm.append(heatmap)
            else:
                if random.random() < 1.:
                    img, points = keypoint_augment.simple_augment(img, points, self.data_type)
                target_size = int(IMAGE_SIZE / DOWN_RATIO)
                heatmap = self.generate_hm(target_size, points, image_size=IMAGE_SIZE, s=np.sqrt(DOWN_RATIO/2))
                batch_image.append(img)
                batch_hm.append(heatmap)

            if len(batch_image) >= self.batch_size:
                batch_image = batch_image[:self.batch_size]
                batch_hm = batch_hm[:self.batch_size]
                yield batch_image, batch_hm
        if len(batch_image) > 0:
            yield batch_image, batch_hm

annotation = []
with open(ANNOTATION, 'r') as f:
    annotation = f.readlines()

np.random.seed(10101)
np.random.shuffle(annotation)
np.random.seed(None)

data = Dataset(annotation[:100], data_type='train', batch_size=32)

for batch_img, batch_hm in data.gen():
    for i in range(len(batch_img)):
        img = batch_img[i]
        hm = batch_hm[i]
        plt.subplot(1, 6, 1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        for j in range(4):
            hmj = hm[:, :, j]
            plt.subplot(1, 6, j + 2), plt.imshow(hmj)
        plt.subplot(1, 6, 6), plt.imshow(np.sum(hm, axis=-1))
        plt.show()
