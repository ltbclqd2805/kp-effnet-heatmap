import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np 
import cv2

from model import Model, Mobilenetv3Model
from const import *

# eff-b4-realdata: 106.54 99.62 | 18.1197
# eff-b0-realdata:				| 19.88
# eff-b4-fixed-BN: 125.10       | 21.5014
# eff-b0-fixed-BN: 112.28       | 20.3787
# mbnv3s-realsynthetic:			| 22.0651

def predict_on_image(model, image_path):
    result = []
    img = cv2.imread(image_path)
    height, width = img.shape[:2]
    r_h = height * 1.0 / IMAGE_SIZE
    r_w = width * 1.0 / IMAGE_SIZE
    img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
    img = np.expand_dims(img, axis=0)
    heatmap, scores, classes = model.predict(img / 255.0)
    for i in range(NUM_CLASSES):
        mask = (classes == i).astype(int)
        max_score = np.max(scores * mask)
        idx_max = np.array(np.where(scores == max_score))
        xs = (idx_max[2, 0] * DOWN_RATIO + DOWN_RATIO / 2) # * r_w
        ys = (idx_max[1, 0] * DOWN_RATIO + DOWN_RATIO / 2) # * r_h
        result.append((xs, ys))
    return result

def cal_loss(label, predicted, r_h, r_w):
    loss = 0
    for i in range(len(label)):
        label_box = label[i]
        predicted_box = predicted[i]
        x, y = label_box
        u, v = predicted_box
        x /= r_w
        y /= r_h
        loss += np.sqrt((x - u) * (x - u) + (y - v) * (y - v))
    print(loss)
    return loss

with tf.Session(config=tf.ConfigProto(allow_soft_placement=False)) as sess:
    model = Model(ann_train=None, ann_valid=None, session=sess, phase='test')

    total_loss = 0
    cnt = 0
    with open('test_data_kp.txt', 'r') as f:
        for line in f:
            line = line[:-1].split(' ')
            image_path = line[0] + '.jpg'
            label = [(int(line[2 * i + 1]), int(line[2 * i + 2])) for i in range(4)]

            img = cv2.imread(image_path)
            height, width = img.shape[:2]
            r_h = height * 1.0 / IMAGE_SIZE
            r_w = width * 1.0 / IMAGE_SIZE

            predicted = predict_on_image(model, image_path)
            cnt += 1
            total_loss += cal_loss(label, predicted, r_h, r_w)

    print('Loss: ', total_loss / cnt)
