import os
import requests
import json
import logzero
import time
import base64
import shutil
import math

from datetime import datetime
from argparse import ArgumentParser
from logzero import logger


def parse_arguments():
    parser = ArgumentParser()
    
    parser.add_argument('inp_dir')
    parser.add_argument('--url', default='http://api.vdsense.com')
    parser.add_argument('-o', '--out', default='outputs')
    parser.add_argument('-b', '--batch-size', type=int, default=1)
    parser.add_argument('-e', '--exclude', default=None)
    parser.add_argument('--retries', type=int, default=3)
    parser.add_argument('--no-errors', dest='skip_errors', action='store_false')
    parser.add_argument('--no-debug', action='store_true')
    parser.add_argument('--timeout', type=int, default=120)

    return parser.parse_args()


def create_request(path):
    with open(path, 'rb') as f:
        sb64 = base64.b64encode(f.read()).decode()

    return {
        'image': {
            'content': sb64
        },
        'features': {
            'type': 'IDENTITY_CARD_DETECTION'
        }
    }


def is_image(path):
    ext = path.split('.')[-1]
    fname = os.path.split(path)[-1]
    return ext.lower() in ['jpg', 'png', 'jpeg'] and not fname.startswith('.')


def get_image_id(path):
    return os.path.split(path)[-1].split('.')[0]


if __name__ == "__main__":
    args = parse_arguments()
    job_id = datetime.now().strftime('%Y-%m-%d_%Hh%Mm%Ss')
    logzero.logfile(os.path.join('logs', '%s.log' % job_id),
        mode='w')
    os.makedirs(args.out, exist_ok=True)

    exclude_list = []
    if args.exclude:
        logger.info('Excluding patterns in %s' % args.exclude)
        with open(args.exclude, 'rt') as f:
            for line in f:
                line = line.strip()
                exclude_list.append(line)

    def not_excluded(path):
        for p in exclude_list:
            if p in path:
                return False
        return True

    logger.info('Job ID: %s' % job_id)
    logger.info('Fetching images from %s' % args.inp_dir)
    img_paths = os.listdir(args.inp_dir)
    img_paths = filter(is_image, img_paths)
    img_paths = filter(not_excluded, img_paths)
    img_paths = map(lambda x: os.path.join(args.inp_dir, x), img_paths)
    img_paths = list(img_paths)

    url = args.url + '/v1/images:annotate'
    num_batches = math.ceil(len(img_paths) / args.batch_size)
    logger.info('Processing %d images (%d batches)' % (len(img_paths), num_batches))

    try:
        for i in range(num_batches):
            batch = img_paths[args.batch_size * i:args.batch_size * (i+1)]
            print(batch)
            reqs = list(map(create_request, batch))
            ids = list(map(get_image_id, batch))

            logger.info('Batch %d/%d' %(i+1, num_batches)) 
            logger.info('IDs: %s' % (', '.join(ids)))
            logger.info('Making request to %s' % url)

            reqf = {
                'requests': reqs
            }
            if not args.no_debug:
                reqf['debug'] = {
                    'job_id': '_'.join(ids)
                }

            r = None
            print(reqf.keys())
            for i in range(args.retries + 1):
                if i > 0:
                    logger.warning('Retrying (%d/%d)' % (i, args.retries))
                
                req_start = datetime.now()
                try:
                    r = requests.post(url, json=reqf, timeout=args.timeout)
                except requests.exceptions.Timeout:
                    logger.error('Request timed out')
                    break
                req_time = datetime.now() - req_start

                if r.status_code != 200:
                    logger.error('Request failed with status code %d' % r.status_code)
                else:
                    break
            
            if r is None or r.status_code != 200:
                if args.skip_errors:
                    continue
                else:
                    logger.error('Exiting due to errors')
                    exit_code = 11 if r is None else r.status_code
                    exit(exit_code)

            responses = r.json()['responses']
            logger.info('Request completed in %ds' % req_time.total_seconds())

            for j, (id_, resp) in enumerate(zip(ids, responses)):
                id_dir = os.path.join(args.out, id_)
                os.makedirs(id_dir, exist_ok=True)
                resp_path = os.path.join(id_dir, 'response_%s.json' % job_id)
                extras_path = os.path.join(id_dir, 'extras_%s.json' % job_id)
                im_path = os.path.join(id_dir, 'original.jpg')
                with open(resp_path, 'w', encoding="utf-8") as f:
                    f.write(json.dumps(resp, indent=2, ensure_ascii=False))
                #with open(resp_path, 'wt') as f:
                    #f.write(json.dumps(resp, indent=2, ensure_ascii=False).encode('utf-8').decode())

                extras = {
                    'totalSecs': req_time.total_seconds(),
                    'batchSize': args.batch_size,
                    'host': args.url
                }
                with open(extras_path, 'wt') as f:
                    json.dump(extras, f, indent=2)
                shutil.copy2(batch[j], im_path)
            logger.info('')
            
        logger.info('Finished')
    except KeyboardInterrupt:
        logger.warning('Interrupted.')
        exit(-1)
