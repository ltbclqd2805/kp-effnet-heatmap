import os

SRC_DATA_DIR = '../Backside'
ANNOTATION = os.path.join(SRC_DATA_DIR, '2sides_kp_with_synthetic.txt')
IMAGE_SIZE = 224
DOWN_RATIO = 8
NUM_CLASSES = 4

WEIGHT_DECAY = 1e-5
LNR = 1e-3
MODEL_DIR = 'checkpoints/effb4-realdata-bifpn-r1-8-wd5e-7'
LOAD_MODEL = True
BATCH_SIZE = 64
NUM_EPOCHS = 100
