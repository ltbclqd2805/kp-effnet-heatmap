import efficientnet_builder
import mobilenet_v3
import vgg
import tensorflow as tf
import os
import logging
import time
import numpy as np
from const import *
from data_provider import Dataset

class Model:
    def __init__(self, session, ann_train, ann_valid, phase='train'):
        self.data_format = 'channels_last'
        self.weights_decay = WEIGHT_DECAY
        self.global_step = tf.get_variable(name='global_step', initializer=tf.constant(0), trainable=False)
        self.model_dir = MODEL_DIR
        if not os.path.isdir(self.model_dir):
            os.makedirs(self.model_dir)
        self.load_model = LOAD_MODEL
        self.phase = phase
        self.num_epochs = NUM_EPOCHS
        self.batch_size = BATCH_SIZE

        self.train_data = Dataset(ann_train, 'train', self.batch_size)
        self.valid_data = Dataset(ann_valid, 'valid', self.batch_size)

        self._define_input()
        self._build_wbifpn()

        self.sess = session
        self.saver_all = tf.train.Saver(tf.all_variables(), max_to_keep=None)
        self.checkpoint_path = os.path.join(self.model_dir, "model.ckpt")

        self.best_valid = 1e10
        if os.path.exists(os.path.join(self.model_dir, 'best_valid_loss.txt')):
            with open(os.path.join(self.model_dir, 'best_valid_loss.txt'), 'r') as f:
                self.best_valid = float(f.read())

        ckpt = tf.train.get_checkpoint_state(self.model_dir)
        if ckpt and self.load_model:
            if self.phase != "train":
                print("Reading model parameters from %s", "best_valid_model.ckpt")
                self.saver_all.restore(self.sess, os.path.join(self.model_dir, "best_valid_model.ckpt"))
            else:
                print("Reading model parameters from %s", ckpt.model_checkpoint_path)
                self.saver_all.restore(self.sess, ckpt.model_checkpoint_path)

        else:
            print("Created model with fresh parameters.")
            self.sess.run(tf.initialize_all_variables())

    def _define_input(self):
        self.images = tf.placeholder(tf.float32, [None, IMAGE_SIZE, IMAGE_SIZE, 3])
        self.target_size = int(IMAGE_SIZE / DOWN_RATIO)
        self.true_hm = tf.placeholder(tf.float32, [None, self.target_size, self.target_size, NUM_CLASSES])
        self.is_training = tf.placeholder(tf.bool)
        self.lr = tf.placeholder(tf.float32)
    
    def _build(self):
        features, endpoints = efficientnet_builder.build_model_base(self.images, model_name='efficientnet-b4', training=self.is_training)
        # features, endpoints = vgg.build_model_base(self.images, model_name='efficientnet-b0', training=self.is_training)
        with tf.variable_scope('upsampling'):
            stage5 = self._bn_swish_conv(features, 256, 1, 1) # 7 x 7
            stage5_4 = self._bn_swish_dconv(stage5, 256, 4, 2) # 14 x 14
            stage5_3 = self._bn_swish_dconv(stage5_4, 256, 4, 2) # 28 x 28

            stage4 = self._bn_swish_conv(endpoints['reduction_4'], 256, 1, 1) # 14 x 14
            stage4_3 = self._bn_swish_conv(stage4 + stage5_4, 256, 3, 1) # 14 x 14
            stage4_3 = self._bn_swish_dconv(stage4_3, 256, 4, 2) # 28 x 28

            stage3 = self._bn_swish_conv(endpoints['reduction_3'], 256, 1, 1) # 28 x 28
            features = self._bn_swish_conv(stage3 + stage4_3 + stage5_3, 256, 3, 1)
            features = self._bn_swish_conv(stage3 + stage4_3 + stage5_3, 256, 1, 1)

        self.kp_prediction = self._bn_swish_conv(features, NUM_CLASSES, 3, 1)
        self.kp_prediction = tf.nn.sigmoid(self.kp_prediction)

        # self.offset_prediction = self._bn_swish_conv(features, 2, 3, 1)

        print('Keypoint shape: ', self.kp_prediction.get_shape())

        self.pos_loss, self.neg_loss, self.debug_loss = self._focal_loss(self.kp_prediction, self.true_hm)
        self.l2_loss = self.weights_decay * tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])

        self.loss = self.pos_loss + self.neg_loss + self.l2_loss
        optimizer = tf.train.AdamOptimizer(self.lr)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_op = optimizer.minimize(self.loss, global_step=self.global_step)

        # Decode heatmap prediction
        hm_peak = tf.layers.max_pooling2d(self.kp_prediction, 3, 1, 'same')
        mask = tf.cast(tf.equal(hm_peak, self.kp_prediction), tf.float32)
        hm = hm_peak * mask
        print('Shape after multiply mask: ', hm.get_shape())
        self.scores = tf.reduce_max(hm, axis=-1)
        self.classes = tf.argmax(hm, axis=-1)
        print('Scores shape', self.scores.get_shape())
        print('Classes shape', self.classes.get_shape())

    def _build_wbifpn(self):
        features, endpoints = efficientnet_builder.build_model_base(self.images, model_name='efficientnet-b4', training=self.is_training)
        # features, endpoints = vgg.build_model_base(self.images, model_name='efficientnet-b0', training=self.is_training)
        with tf.variable_scope('upsampling'):
            # top-down path
            P5_in = self._bn_swish_conv(features, 256, 1, 1) # 7 x 7 x 256
            P4_in = self._bn_swish_conv(endpoints['reduction_4'], 256, 1, 1) # 14 x 14 x 256
            P3_in = self._bn_swish_conv(endpoints['reduction_3'], 256, 1, 1) # 28 x 28 x 256

            P5_u = self._bn_swish_dconv(P5_in, 256, 4, 2) # 14 x 14 x 256 
            P4_td = self.wbifpn_layer([P4_in, P5_u], 'P4_td')

            P4_u = self._bn_swish_dconv(P4_td, 256, 4, 2) # 28 x 28 x 256 
            P3_out = self.wbifpn_layer([P3_in, P4_u], 'P3_out')

            # bottom-up path
            P3_d = tf.layers.max_pooling2d(P3_out, 2, 2, 'same')
            P4_out = self.wbifpn_layer([P3_d, P4_td, P4_in], 'P4_out')

            P4_d = tf.layers.max_pooling2d(P4_out, 2, 2, 'same')
            P5_out = self.wbifpn_layer([P4_d, P5_in], 'P5_out')

            # auxiliary path
            P5_out_u = self._bn_swish_dconv(P5_out, 256, 4, 2)
            P4_out_aux = self.wbifpn_layer([P4_out, P5_out_u], 'P4_out_aux')
            P4_out_aux_u = self._bn_swish_dconv(P4_out_aux, 256, 4, 2)
            P3_out_aux = self.wbifpn_layer([P3_out, P4_out_aux_u], 'P3_out_aux')

            features = self._bn_swish_conv(P3_out_aux, 256, 1, 1)

        self.kp_prediction = self._bn_swish_conv(features, NUM_CLASSES, 3, 1)
        self.kp_prediction = tf.nn.sigmoid(self.kp_prediction)

        # self.offset_prediction = self._bn_swish_conv(features, 2, 3, 1)

        print('Keypoint shape: ', self.kp_prediction.get_shape())

        self.pos_loss, self.neg_loss, self.debug_loss = self._focal_loss(self.kp_prediction, self.true_hm)
        self.l2_loss = self.weights_decay * tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])

        self.loss = self.pos_loss + self.neg_loss + self.l2_loss
        optimizer = tf.train.AdamOptimizer(self.lr)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_op = optimizer.minimize(self.loss, global_step=self.global_step)

        # Decode heatmap prediction
        hm_peak = tf.layers.max_pooling2d(self.kp_prediction, 3, 1, 'same')
        mask = tf.cast(tf.equal(hm_peak, self.kp_prediction), tf.float32)
        hm = hm_peak * mask
        print('Shape after multiply mask: ', hm.get_shape())
        self.scores = tf.reduce_max(hm, axis=-1)
        self.classes = tf.argmax(hm, axis=-1)
        print('Scores shape', self.scores.get_shape())
        print('Classes shape', self.classes.get_shape())


    def wbifpn_layer(self, features, name, epsilon=1e-4):
        with tf.variable_scope(name):
            weights = []
            n = len(features)

            for i in range(n):
                weights.append(tf.nn.relu(tf.get_variable(name='W_{}'.format(i), initializer=1.0/n)))

            x = tf.reduce_sum([weights[i] * features[i] for i in range(n)], axis=0)
            x = x / (tf.reduce_sum(weights) + epsilon)

        return x

    def train(self):
        current_step = self.sess.run(self.global_step)

        for epoch in range(self.num_epochs):
            num_samples = 0
            loss = 0
            pos_loss = 0
            neg_loss = 0
            l2_loss = 0
            for batch_img, batch_hm in self.train_data.gen():
                num_samples += len(batch_img)
                start_time = time.time()
                current_step += 1
                feed_dict = {}
                feed_dict[self.images] = batch_img
                feed_dict[self.true_hm] = batch_hm
                feed_dict[self.is_training] = True
                feed_dict[self.lr] = LNR
                cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss, debug, _ = \
                    self.sess.run([self.loss, self.pos_loss, self.neg_loss, self.l2_loss, self.debug_loss, self.train_op], \
                        feed_dict=feed_dict)
                loss += cur_loss * len(batch_img)
                pos_loss += cur_pos_loss * len(batch_img)
                neg_loss += cur_neg_loss * len(batch_img)
                l2_loss += cur_l2_loss * len(batch_img)
                cur_step_time = (time.time() - start_time)
                if current_step % 500 == 1:
                    print('Epoch %d step %d %.3fs, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f' \
                        %(epoch, current_step, cur_step_time, cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss))
                    # print(debug)
            print('End of epoch %d, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f'\
                 %(epoch, loss / num_samples, pos_loss / num_samples, neg_loss / num_samples, l2_loss / num_samples))

            num_samples = 0
            loss = 0
            pos_loss = 0
            neg_loss = 0
            l2_loss = 0
            for batch_img, batch_hm in self.valid_data.gen():
                num_samples += len(batch_img)
                feed_dict = {}
                feed_dict[self.images] = batch_img
                feed_dict[self.true_hm] = batch_hm
                feed_dict[self.is_training] = False
                cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss = \
                    self.sess.run([self.loss, self.pos_loss, self.neg_loss, self.l2_loss], feed_dict=feed_dict)
                loss += cur_loss * len(batch_img)
                pos_loss += cur_pos_loss * len(batch_img)
                neg_loss += cur_neg_loss * len(batch_img)
                l2_loss += cur_l2_loss * len(batch_img)
            print('Valid at epoch %d, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f'\
                 %(epoch, loss / num_samples, pos_loss / num_samples, neg_loss / num_samples, l2_loss / num_samples))

            if loss / num_samples < self.best_valid:
                with open(os.path.join(self.model_dir, 'best_valid_loss.txt'), 'w') as f:
                    f.write(str(loss / num_samples))
                self.best_valid = loss / num_samples
                print("Saving new best valid model.")
                self.saver_all.save(self.sess, os.path.join(self.model_dir, 'best_valid_model.ckpt'))


            self.saver_all.save(self.sess, self.checkpoint_path, global_step=self.global_step)

    def predict(self, images):
        feed_dict = {}
        feed_dict[self.images] = images
        feed_dict[self.is_training] = False
        heatmap, scores, classes = self.sess.run([self.kp_prediction, self.scores, self.classes], feed_dict=feed_dict)

        return heatmap, scores, classes

    def _focal_loss(self, prediction, label):
        pos_idx = tf.cast(tf.equal(label, 1), tf.float32)
        neg_idx = tf.cast(tf.less(label, 1), tf.float32)

        neg_weights = tf.pow(tf.clip_by_value(1 - label, 1e-4, 1 - 1e-4), 4)
        prediction = tf.clip_by_value(prediction, 1e-4, 1 - 1e-4)

        pos_loss = tf.log(prediction) * tf.pow(1 - prediction, 2) * pos_idx
        neg_loss = tf.log(1 - prediction) * tf.pow(prediction, 2) * neg_weights * neg_idx

        num_pos = tf.reduce_sum(pos_idx, axis=[1, 2, 3])

        # DEBUG
        old_pos_loss = tf.log(prediction)

        pos_loss = tf.reduce_mean(-tf.reduce_sum(pos_loss, axis=[1, 2, 3]) / num_pos)
        neg_loss = tf.reduce_mean(-tf.reduce_sum(neg_loss, axis=[1, 2, 3]) / num_pos)
        total_loss = pos_loss + neg_loss

        return pos_loss, neg_loss, -tf.reduce_sum(old_pos_loss, axis=[1, 2, 3]) * 0.25

    def _mse_loss(self, prediction, label):
        return tf.reduce_mean(tf.squared_difference(prediction, label))

    def _bn(self, bottom):
        bn = tf.layers.batch_normalization(
            inputs=bottom,
            axis=3 if self.data_format == 'channels_last' else 1,
            training=self.is_training
        )
        return bn

    def _bn_swish_conv(self, inputs, filters, kernel_size, strides):
        inputs = self._bn(inputs)
        inputs = tf.nn.swish(inputs)
        inputs = tf.layers.conv2d(
            inputs=inputs,
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding='same',
            data_format=self.data_format
        )
        return inputs

    def _bn_swish_dconv(self, inputs, filters, kernel_size, strides):
        inputs = self._bn(inputs)
        inputs = tf.nn.swish(inputs)
        inputs = tf.layers.conv2d_transpose(
            inputs=inputs,
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding='same',
            data_format=self.data_format
        )
        return inputs


class Mobilenetv3Model:
    def __init__(self, session, ann_train, ann_valid, phase='train'):
        self.data_format = 'channels_last'
        self.weights_decay = WEIGHT_DECAY
        self.global_step = tf.get_variable(name='global_step', initializer=tf.constant(0), trainable=False)
        self.model_dir = MODEL_DIR
        if not os.path.isdir(self.model_dir):
            os.makedirs(self.model_dir)
        self.load_model = LOAD_MODEL
        self.phase = phase
        self.num_epochs = NUM_EPOCHS
        self.batch_size = BATCH_SIZE

        self.train_data = Dataset(ann_train, 'train', self.batch_size)
        self.valid_data = Dataset(ann_valid, 'valid', self.batch_size)

        self._define_input()
        self._build()

        self.sess = session
        self.saver_all = tf.train.Saver(tf.all_variables(), max_to_keep=None)
        self.checkpoint_path = os.path.join(self.model_dir, "model.ckpt")

        self.best_valid = 1e10
        if os.path.exists(os.path.join(self.model_dir, 'best_valid_loss.txt')):
            with open(os.path.join(self.model_dir, 'best_valid_loss.txt'), 'r') as f:
                self.best_valid = float(f.read())

        ckpt = tf.train.get_checkpoint_state(self.model_dir)
        if ckpt and self.load_model:
            if self.phase != "train":
                print("Reading model parameters from %s", "best_valid_model.ckpt")
                self.saver_all.restore(self.sess, os.path.join(self.model_dir, "best_valid_model.ckpt"))
            else:
                print("Reading model parameters from %s", ckpt.model_checkpoint_path)
                self.saver_all.restore(self.sess, ckpt.model_checkpoint_path)

        else:
            print("Created model with fresh parameters.")
            self.sess.run(tf.initialize_all_variables())

    def _define_input(self):
        self.images = tf.placeholder(tf.float32, [None, IMAGE_SIZE, IMAGE_SIZE, 3])
        self.target_size = int(IMAGE_SIZE / DOWN_RATIO)
        self.true_hm = tf.placeholder(tf.float32, [None, self.target_size, self.target_size, NUM_CLASSES])
        self.is_training = tf.placeholder(tf.bool)
        self.lr = tf.placeholder(tf.float32)
    
    def _build(self):
        features, endpoints = mobilenet_v3.mobilenet(
                                self.images,
                                conv_defs=mobilenet_v3.V3_SMALL,
                                base_only=True,
                                final_endpoint='layer_13')
        print(endpoints)
        with tf.variable_scope('upsampling'):
            stage5 = self._bn_swish_conv(features, 256, 1, 1) # 7 x 7
            stage5_4 = self._bn_swish_dconv(stage5, 256, 4, 2) # 14 x 14
            stage5_3 = self._bn_swish_dconv(stage5_4, 256, 4, 2) # 28 x 28

            stage4 = self._bn_swish_conv(endpoints['layer_9'], 256, 1, 1) # 14 x 14
            stage4_3 = self._bn_swish_conv(stage4 + stage5_4, 256, 3, 1) # 14 x 14
            stage4_3 = self._bn_swish_dconv(stage4_3, 256, 4, 2) # 28 x 28

            stage3 = self._bn_swish_conv(endpoints['layer_4'], 256, 1, 1) # 28 x 28
            features = self._bn_swish_conv(stage3 + stage4_3 + stage5_3, 256, 3, 1)
            features = self._bn_swish_conv(stage3 + stage4_3 + stage5_3, 256, 1, 1)

        self.kp_prediction = self._bn_swish_conv(features, NUM_CLASSES, 3, 1)
        self.kp_prediction = tf.nn.sigmoid(self.kp_prediction)

        # self.offset_prediction = self._bn_swish_conv(features, 2, 3, 1)

        print('Keypoint shape: ', self.kp_prediction.get_shape())

        self.pos_loss, self.neg_loss, self.debug_loss = self._focal_loss(self.kp_prediction, self.true_hm)
        self.l2_loss = self.weights_decay * tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])

        self.loss = self.pos_loss + self.neg_loss + self.l2_loss
        optimizer = tf.train.AdamOptimizer(self.lr)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_op = optimizer.minimize(self.loss, global_step=self.global_step)

        # Decode heatmap prediction
        hm_peak = tf.layers.max_pooling2d(self.kp_prediction, 3, 1, 'same')
        mask = tf.cast(tf.equal(hm_peak, self.kp_prediction), tf.float32)
        hm = hm_peak * mask
        print('Shape after multiply mask: ', hm.get_shape())
        self.scores = tf.reduce_max(hm, axis=-1)
        self.classes = tf.argmax(hm, axis=-1)
        print('Scores shape', self.scores.get_shape())
        print('Classes shape', self.classes.get_shape())

    def _build_wbifpn(self):
        features, endpoints = mobilenet_v3.mobilenet(
                                self.images,
                                conv_defs=mobilenet_v3.V3_SMALL,
                                base_only=True,
                                final_endpoint='layer_13')
        with tf.variable_scope('upsampling'):
            # top-down path
            P5_in = self._bn_swish_conv(features, 256, 1, 1) # 7 x 7 x 256
            P4_in = self._bn_swish_conv(endpoints['layer_9'], 256, 1, 1) # 14 x 14 x 256
            P3_in = self._bn_swish_conv(endpoints['layer_4'], 256, 1, 1) # 28 x 28 x 256

            P5_u = self._bn_swish_dconv(P5_in, 256, 4, 2) # 14 x 14 x 256 
            P4_td = self.wbifpn_layer([P4_in, P5_u], 'P4_td')

            P4_u = self._bn_swish_dconv(P4_td, 256, 4, 2) # 28 x 28 x 256 
            P3_out = self.wbifpn_layer([P3_in, P4_u], 'P3_out')

            # bottom-up path
            P3_d = tf.layers.max_pooling2d(P3_out, 2, 2, 'same')
            P4_out = self.wbifpn_layer([P3_d, P4_td, P4_in], 'P4_out')

            P4_d = tf.layers.max_pooling2d(P4_out, 2, 2, 'same')
            P5_out = self.wbifpn_layer([P4_d, P5_in], 'P5_out')

            # auxiliary path
            P5_out_u = self._bn_swish_dconv(P5_out, 256, 4, 2)
            P4_out_aux = self.wbifpn_layer([P4_out, P5_out_u], 'P4_out_aux')
            P4_out_aux_u = self._bn_swish_dconv(P4_out_aux, 256, 4, 2)
            P3_out_aux = self.wbifpn_layer([P3_out, P4_out_aux_u], 'P3_out_aux')

            features = self._bn_swish_conv(P3_out_aux, 256, 1, 1)

        self.kp_prediction = self._bn_swish_conv(features, NUM_CLASSES, 3, 1)
        self.kp_prediction = tf.nn.sigmoid(self.kp_prediction)

        # self.offset_prediction = self._bn_swish_conv(features, 2, 3, 1)

        print('Keypoint shape: ', self.kp_prediction.get_shape())

        self.pos_loss, self.neg_loss, self.debug_loss = self._focal_loss(self.kp_prediction, self.true_hm)
        self.l2_loss = self.weights_decay * tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])

        self.loss = self.pos_loss + self.neg_loss + self.l2_loss
        optimizer = tf.train.AdamOptimizer(self.lr)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_op = optimizer.minimize(self.loss, global_step=self.global_step)

        # Decode heatmap prediction
        hm_peak = tf.layers.max_pooling2d(self.kp_prediction, 3, 1, 'same')
        mask = tf.cast(tf.equal(hm_peak, self.kp_prediction), tf.float32)
        hm = hm_peak * mask
        print('Shape after multiply mask: ', hm.get_shape())
        self.scores = tf.reduce_max(hm, axis=-1)
        self.classes = tf.argmax(hm, axis=-1)
        print('Scores shape', self.scores.get_shape())
        print('Classes shape', self.classes.get_shape())


    def wbifpn_layer(self, features, name, epsilon=1e-4):
        with tf.variable_scope(name):
            weights = []
            n = len(features)

            for i in range(n):
                weights.append(tf.nn.relu(tf.get_variable(name='W_{}'.format(i), initializer=1.0/n)))

            x = tf.reduce_sum([weights[i] * features[i] for i in range(n)], axis=0)
            x = x / (tf.reduce_sum(weights) + epsilon)

        return x

    def train(self):
        current_step = self.sess.run(self.global_step)

        for epoch in range(self.num_epochs):
            num_samples = 0
            loss = 0
            pos_loss = 0
            neg_loss = 0
            l2_loss = 0
            for batch_img, batch_hm in self.train_data.gen():
                num_samples += len(batch_img)
                start_time = time.time()
                current_step += 1
                feed_dict = {}
                feed_dict[self.images] = batch_img
                feed_dict[self.true_hm] = batch_hm
                feed_dict[self.is_training] = True
                feed_dict[self.lr] = LNR
                cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss, debug, _ = \
                    self.sess.run([self.loss, self.pos_loss, self.neg_loss, self.l2_loss, self.debug_loss, self.train_op], \
                        feed_dict=feed_dict)
                loss += cur_loss * len(batch_img)
                pos_loss += cur_pos_loss * len(batch_img)
                neg_loss += cur_neg_loss * len(batch_img)
                l2_loss += cur_l2_loss * len(batch_img)
                cur_step_time = (time.time() - start_time)
                if current_step % 500 == 1:
                    print('Epoch %d step %d %.3fs, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f' \
                        %(epoch, current_step, cur_step_time, cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss))
                    # print(debug)
            print('End of epoch %d, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f'\
                 %(epoch, loss / num_samples, pos_loss / num_samples, neg_loss / num_samples, l2_loss / num_samples))

            num_samples = 0
            loss = 0
            pos_loss = 0
            neg_loss = 0
            l2_loss = 0
            for batch_img, batch_hm in self.valid_data.gen():
                num_samples += len(batch_img)
                feed_dict = {}
                feed_dict[self.images] = batch_img
                feed_dict[self.true_hm] = batch_hm
                feed_dict[self.is_training] = False
                cur_loss, cur_pos_loss, cur_neg_loss, cur_l2_loss = \
                    self.sess.run([self.loss, self.pos_loss, self.neg_loss, self.l2_loss], feed_dict=feed_dict)
                loss += cur_loss * len(batch_img)
                pos_loss += cur_pos_loss * len(batch_img)
                neg_loss += cur_neg_loss * len(batch_img)
                l2_loss += cur_l2_loss * len(batch_img)
            print('Valid at epoch %d, loss %.3f, pos_loss %.3f, neg_loss %.3f, l2_loss %.3f'\
                 %(epoch, loss / num_samples, pos_loss / num_samples, neg_loss / num_samples, l2_loss / num_samples))

            if loss / num_samples < self.best_valid:
                with open(os.path.join(self.model_dir, 'best_valid_loss.txt'), 'w') as f:
                    f.write(str(loss / num_samples))
                self.best_valid = loss / num_samples
                print("Saving new best valid model.")
                self.saver_all.save(self.sess, os.path.join(self.model_dir, 'best_valid_model.ckpt'))


            self.saver_all.save(self.sess, self.checkpoint_path, global_step=self.global_step)

    def predict(self, images):
        feed_dict = {}
        feed_dict[self.images] = images
        feed_dict[self.is_training] = False
        heatmap, scores, classes = self.sess.run([self.kp_prediction, self.scores, self.classes], feed_dict=feed_dict)

        return heatmap, scores, classes

    def _focal_loss(self, prediction, label):
        pos_idx = tf.cast(tf.equal(label, 1), tf.float32)
        neg_idx = tf.cast(tf.less(label, 1), tf.float32)

        neg_weights = tf.pow(tf.clip_by_value(1 - label, 1e-4, 1 - 1e-4), 4)
        prediction = tf.clip_by_value(prediction, 1e-4, 1 - 1e-4)

        pos_loss = tf.log(prediction) * tf.pow(1 - prediction, 2) * pos_idx
        neg_loss = tf.log(1 - prediction) * tf.pow(prediction, 2) * neg_weights * neg_idx

        num_pos = tf.reduce_sum(pos_idx, axis=[1, 2, 3])

        # DEBUG
        old_pos_loss = tf.log(prediction)

        pos_loss = tf.reduce_mean(-tf.reduce_sum(pos_loss, axis=[1, 2, 3]) / num_pos)
        neg_loss = tf.reduce_mean(-tf.reduce_sum(neg_loss, axis=[1, 2, 3]) / num_pos)
        total_loss = pos_loss + neg_loss

        return pos_loss, neg_loss, -tf.reduce_sum(old_pos_loss, axis=[1, 2, 3]) * 0.25

    def _mse_loss(self, prediction, label):
        return tf.reduce_mean(tf.squared_difference(prediction, label))

    def _bn(self, bottom):
        bn = tf.layers.batch_normalization(
            inputs=bottom,
            axis=3 if self.data_format == 'channels_last' else 1,
            training=self.is_training
        )
        return bn

    def _bn_swish_conv(self, inputs, filters, kernel_size, strides):
        inputs = self._bn(inputs)
        inputs = mobilenet_v3.hard_swish(inputs)
        inputs = tf.layers.conv2d(
            inputs=inputs,
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding='same',
            data_format=self.data_format
        )
        return inputs

    def _bn_swish_dconv(self, inputs, filters, kernel_size, strides):
        inputs = self._bn(inputs)
        inputs = mobilenet_v3.hard_swish(inputs)
        inputs = tf.layers.conv2d_transpose(
            inputs=inputs,
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding='same',
            data_format=self.data_format
        )
        return inputs
