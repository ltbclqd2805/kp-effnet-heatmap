import tensorflow as tf
import matplotlib.pyplot as plt 
import numpy as np 
import cv2

from model import Model, Mobilenetv3Model
from const import *

annotation = []
# with open(ANNOTATION, 'r') as f:
#     annotation = f.readlines()
# np.random.seed(10101)
# np.random.shuffle(annotation)
# np.random.seed(None)
with tf.Session(config=tf.ConfigProto(allow_soft_placement=False)) as sess:
    # model = Mobilenetv3Model(ann_train=annotation[:40000], ann_valid=annotation[40000:], session=sess, phase='train')
    # model.train()
    model = Mobilenetv3Model(ann_train=None, ann_valid=None, session=sess, phase='train')
    # model.train()
